<?php
if (isset($object)) {
$viewData = [
'title' => 'Transaksi Pengembalian',
'breadcrumbs' => [
'Transaksi',
$object->kode,
'Pengembalian',
],
];
} else {
$viewData = [
'title' => 'Pinjam Buku',
'breadcrumbs' => [
'Transaksi',
'Tambah',
]
];
}
?>



<?php $__env->startSection('content'); ?>

<?php
if (isset($object)) {
$actionUrl = route('transaksi.update', $object->id);
} else {
$actionUrl = route('transaksi.store');
}
?>
<div class="row">
  <div class="<?php echo e(isset($object) ? "col-md-8" : "col-md-12"); ?>">
    <form action="<?php echo e($actionUrl); ?>" method="POST" enctype="multipart/form-data">

      <?php if(isset($object)): ?>
      <?php echo e(method_field('PATCH')); ?>

      <input type="hidden" name="user_id" value="<?php echo e($object->id); ?>" />
      <?php endif; ?>

      <?php echo e(csrf_field()); ?>


      <div class="card">
        <div class="card-header">
          <h4 class="card-title"><?php echo e($viewData['title']); ?></h4>
        </div>
        <br>
        <div class="card-body">


          <div class="form-body">
            <div class="row">
                
                <?php if(isset($object)): ?>
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Detail Peminjaman</h4>
                    </div>
                    <br>
                    <div class="card-body">
                      <div class="text-center">
                        <img src="<?php echo e(\Storage::url($object->buku_id)); ?>" style="max-width: 50%;" class="rounded-circle img-border box-shadow-1">
                      </div>
                      <br>
              
                      <?php
                      $details = [
                      'anggota_name' => 'Peminjam',
                      'buku_name' => 'Buku',
                      'tgl_pinjam' => 'Tanggal Pinjam',
                      'tgl_hrs_kembali' => 'Sampai Tanggal',
                      ];
                      ?>
              
                      <?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="mt-1">
                        <h6 class="mb-0"><?php echo e($label); ?>:</h6>
                        <p><?php echo e(!is_null($object->$key) ? $object->$key : '-'); ?></p>
                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                

              <div class="col-md-2"></div>
              <div class="col-md-10">
                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Dikembalikan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', $viewData, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\perpuspbofix\perpus\resources\views/transaksi/pengembalian.blade.php ENDPATH**/ ?>