@php
if (isset($object)) {
$viewData = [
'title' => 'Transaksi Pengembalian',
'breadcrumbs' => [
'Transaksi',
$object->kode,
'Pengembalian',
],
];
} else {
$viewData = [
'title' => 'Pinjam Buku',
'breadcrumbs' => [
'Transaksi',
'Tambah',
]
];
}
@endphp

@extends('layouts.app', $viewData)

@section('content')
{{-- Form Start --}}
@php
if (isset($object)) {
$actionUrl = route('transaksi.update', $object->id);
} else {
$actionUrl = route('transaksi.store');
}
@endphp
<div class="row">
  <div class="{{ isset($object) ? "col-md-8" : "col-md-12" }}">
    <form action="{{ $actionUrl }}" method="POST" enctype="multipart/form-data">

      @if (isset($object))
      {{ method_field('PATCH') }}
      <input type="hidden" name="user_id" value="{{ $object->id }}" />
      @endif

      {{ csrf_field() }}

      <div class="card">
        <div class="card-header">
          <h4 class="card-title">{{ $viewData['title'] }}</h4>
        </div>
        <br>
        <div class="card-body">


          <div class="form-body">
            <div class="row">
                {{-- Detail Peminjaman --}}
                @if (isset($object))
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Detail Peminjaman</h4>
                    </div>
                    <br>
                    <div class="card-body">
                      <div class="text-center">
                        <img src="{{ \Storage::url($object->buku_id) }}" style="max-width: 50%;" class="rounded-circle img-border box-shadow-1">
                      </div>
                      <br>
              
                      @php
                      $details = [
                      'anggota_name' => 'Peminjam',
                      'buku_name' => 'Buku',
                      'tgl_pinjam' => 'Tanggal Pinjam',
                      'tgl_hrs_kembali' => 'Sampai Tanggal',
                      ];
                      @endphp
              
                      @foreach ($details as $key => $label)
                      <div class="mt-1">
                        <h6 class="mb-0">{{ $label }}:</h6>
                        <p>{{ !is_null($object->$key) ? $object->$key : '-' }}</p>
                      </div>
                      @endforeach
              
                    </div>
                  </div>
                </div>
                @endif
                {{-- Detail Peminjaman --}}

              <div class="col-md-2"></div>
              <div class="col-md-10">
                <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Dikembalikan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

@endsection